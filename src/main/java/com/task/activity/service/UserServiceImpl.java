package com.task.activity.service;


import com.task.activity.dao.UserRepository;
import com.task.activity.model.User;
import com.task.activity.utilities.DateTimeFormat;
import com.task.activity.utilities.EmailValidator;
import com.task.activity.utilities.PhoneValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailValidator emailValidator;
    @Autowired
    private PhoneValidator phoneValidator;

    @Autowired
    private DateTimeFormat dateTimeFormat;

    @Override
    public ResponseEntity UpdateUser(String userID, User userUpdate) {
        Optional<User> user = userRepository.findByUserID(userID);

        if (!user.isPresent()) {
            log.info("User does not exist", userUpdate);
            return new ResponseEntity<>("User does not ayush srijib", HttpStatus.EXPECTATION_FAILED);
        }
        else if ((userUpdate.getFirst_name() == null) || (userUpdate.getFirst_name().matches("\\s*")))
        {
            log.info("No First Name given", userUpdate);
            return new ResponseEntity<>("No First Name given",HttpStatus.BAD_REQUEST);
        }
        else if (!phoneValidator.isValidPhone(userUpdate.getPhone())) {
            log.info("Wrong Phone format", userUpdate);
            return new ResponseEntity<>("Wrong Phone format",HttpStatus.BAD_REQUEST);

        }
        else if (!emailValidator.isValidEmail(userUpdate.getEmail())) {
            log.info("Wrong Email format", userUpdate);
            return  new ResponseEntity("Wrong Email format",HttpStatus.BAD_REQUEST);
        }
        else if ( (!Objects.equals(user.get().getPhone(), userUpdate.getPhone())) && (userRepository.findByPhone(userUpdate.getPhone()).isPresent())) {
            log.info("Phone already Exists", userUpdate);
            return new ResponseEntity("Phone already Exists",HttpStatus.EXPECTATION_FAILED);

        }
        else if ((!Objects.equals(user.get().getEmail(), userUpdate.getEmail())) && (userRepository.findByEmail(userUpdate.getEmail()).isPresent())) {
            log.info("Email already Exists", userUpdate);
            return new ResponseEntity("Email already Exists",HttpStatus.EXPECTATION_FAILED);

        }

        userUpdate.setUserID(userID);
        userUpdate.setUserHash(user.get().getUserHash());
        userUpdate.setCreatedAt(user.get().getCreatedAt());
        User userSaved = userRepository.save(userUpdate);

        log.info("User Updated Successfully", userSaved);
        return new ResponseEntity<>(userSaved,HttpStatus.OK);
    }

    @Override
    public ResponseEntity AddUser(User user){
        if (!phoneValidator.isValidPhone(user.getPhone())) {
            log.info("Wrong Phone format", user);
            return new ResponseEntity<>("Wrong Phone format",HttpStatus.BAD_REQUEST);

        }
        else if ((user.getFirst_name() == null) || (user.getFirst_name().matches("\\s*")))
        {
            log.info("No First Name given", user);
            return new ResponseEntity<>("No First Name given",HttpStatus.BAD_REQUEST);
        }
        else if (!emailValidator.isValidEmail(user.getEmail())) {
            log.info("Wrong Email format", user);
            return  new ResponseEntity("Wrong Email format",HttpStatus.BAD_REQUEST);
        }
        else if (userRepository.findByPhone(user.getPhone()).isPresent()) {
            log.info("Phone already Exists", user);
            return new ResponseEntity("Phone already Exists",HttpStatus.EXPECTATION_FAILED);

        }
        else if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            log.info("Email already Exists", user);
            return new ResponseEntity("Email already Exists",HttpStatus.EXPECTATION_FAILED);

        }
        else if(userRepository.findByUserID(user.getUserID()).isPresent())
        {
            log.info("User already Exists", user);
            return new ResponseEntity<>("User already Exists",HttpStatus.EXPECTATION_FAILED);
        }
        user.setCreatedAt(dateTimeFormat.getCurrentLocalDateTime());
        log.info(user.toString());
        User userSaved = userRepository.insert(user);

        log.info("User Successfully Added", user);
        return new ResponseEntity<>(userSaved,HttpStatus.OK);
    }

    @Override
    public ResponseEntity FetchAllUsers() {
        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList :: add);
        log.info("All Users fetched");
        return new ResponseEntity<>(userList,HttpStatus.OK);
    }

    @Override
    public ResponseEntity DeleteUser(String userID){
        Optional<User> user = userRepository.findByUserID(userID);
        if (!user.isPresent()) {

            log.info("User does not exist for UserID:"+userID);
            return new ResponseEntity<>("User does not exist",HttpStatus.EXPECTATION_FAILED);
        }
        userRepository.deleteUserByUserID(userID);
        log.info("User Deleted Successfully", user.get());
        return new ResponseEntity("Deleted",HttpStatus.OK);
    }

    @Override
    public ResponseEntity FetchUserByUserID(String userID) {
        Optional<User> user = userRepository.findByUserID(userID);
        if (!user.isPresent()) {
            log.info("User does not exist for UserID: "+userID);
            return new ResponseEntity<>("User does not exist",HttpStatus.EXPECTATION_FAILED);
        }
        log.info("User fetched", user.get());
        return new ResponseEntity<>(user.get(),HttpStatus.OK);
    }

    @Override

    public ResponseEntity deleteAll() {
        userRepository.deleteAll();
        log.info("Deleted all users");
        return new ResponseEntity<>("Deleted all Data", HttpStatus.OK);
    }

    public ResponseEntity FetchUserByPhone(String phone) {
        Optional<User> user = userRepository.findByPhone(phone);
        if (!phoneValidator.isValidPhone(phone)) {
            log.info("Wrong Phone format"+phone);
            return new ResponseEntity<>("Wrong Phone format",HttpStatus.BAD_REQUEST);

        }
        else if (!user.isPresent()) {
            log.info("User does not exist");
            return new ResponseEntity<>("User does not exist",HttpStatus.EXPECTATION_FAILED);
        }
        log.info("User fetched", user.get());
        return new ResponseEntity<>(user.get(),HttpStatus.OK);

    }
}