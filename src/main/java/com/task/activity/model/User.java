package com.task.activity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Document(collection = "user")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Indexed(unique = true)
    private String userHash = UUID.randomUUID().toString();
//twst
    @Id
    private String userID;
    private String salutation;
    private String first_name;
    private String last_name;
    private String gender;
    private String state;
    private String phone;
    private String email;
    private Date dob;
    private LocalDateTime createdAt;
    @LastModifiedDate
    private LocalDateTime lastModifiedAt;


}
