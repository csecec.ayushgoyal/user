package com.task.activity.utilities;


import org.springframework.stereotype.Component;

@Component
public class EmailValidator {

    public boolean isValidEmail(String email){
        String emailRegex = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";
        return email != null && email.matches(emailRegex);

    }
}
